using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private SoundManager soundManager;
    
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject winInterface;
    [SerializeField] private GameObject winAtFirstStageInterface;
    [SerializeField] private GameObject loseInterface;
    [SerializeField] private GameObject firePos;

    private Scene thisActiveScene;
    private int normalTime = 1;
    private int freezeTime = 0;

    void Start()
    {
        thisActiveScene = SceneManager.GetActiveScene();
        soundManager.gameTheme.Play();
        Time.timeScale = normalTime;
        winInterface.SetActive(false);
        loseInterface.SetActive(false);
        winAtFirstStageInterface.SetActive(false);
    }
    
    void Update()
    {
        Win();
        Lose();
        WinAtStageOne();
    }
    
    private void Win()
    {
        if(thisActiveScene.name == "4_GameScene02")
        {
            if (scoreManager.score >= scoreManager.maxStarScore)
            {
                winInterface.SetActive(true);
                Time.timeScale = freezeTime;
                firePos.SetActive(false);
                Debug.Log("Win");
            }
        }
        
    }

    private void WinAtStageOne()
    {
        if(thisActiveScene.name == "3_GameScene01")
        {
            if (scoreManager.score >= scoreManager.maxStarScore)
            {
                winAtFirstStageInterface.SetActive(true);
                Time.timeScale = freezeTime;
                firePos.SetActive(false);
                Debug.Log("WinAtStageOne");
            }
        }
        
    }

    private void Lose()
    {
        if (player == null)
        {
            loseInterface.SetActive(true);
            Time.timeScale = freezeTime;
            firePos.SetActive(false);
            Debug.Log("Game Over");
        }
    }

}
