using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] public int maxStarScore;
    [SerializeField] public int score;
        
    [SerializeField] private TextMeshProUGUI textDisplayStarScore;
    
    void Start()
    {
            
    }


    void Update()
    {
        DisplayStarScore();
    }
    
    void DisplayStarScore()
    {
        textDisplayStarScore.text = $"{score}/{maxStarScore}";
    }
    
}
