using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] public AudioSource walkSound;
    [SerializeField] public AudioSource jumpSound;
    [SerializeField] public AudioSource dieSound;
    [SerializeField] public AudioSource keepItemsSound;
    [SerializeField] public AudioSource fireSound;
    [SerializeField] public AudioSource gameTheme;
    
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
}
