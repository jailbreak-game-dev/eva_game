using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Object = System.Object;

public class Bee : MonoBehaviour
{
    [SerializeField] private SoundManager soundManager;
    
    [SerializeField] private Transform playerTransform;
    [SerializeField] private GameObject star;

    [SerializeField] private float enemySpeed;
    [SerializeField] private int hp;

    private Animator am;
    
    private float chasingThresholdDistance = 1.0f;

    private void Start()
    {
        am = GetComponent<Animator>();
    }

    void Update()
    {
        MoveToPlayer();
        BeeDie();
    }
    
    private void MoveToPlayer()
    {
        Vector3 enemyPosition = transform.position;
        Vector3 playerPosition = playerTransform.position;
        enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position
            
        Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
        Vector3 directionToTarget = vectorToTarget.normalized;
        Vector3 velocity = directionToTarget * enemySpeed;
                       
        float distanceToTarget = vectorToTarget.magnitude;

        if (distanceToTarget > chasingThresholdDistance)
        {
            transform.Translate(velocity * Time.deltaTime);
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Fireball")
        {
            hp--;
        }
    }

    private void BeeDie()
    {
        if (hp <= 0)
        {
            StartCoroutine(DieAction());
        }
    }
    
    private IEnumerator DieAction()
    {
        am.SetBool("Die",true);

        yield return new WaitForSeconds(0.50f);
        
        Instantiate(star, transform.position, transform.rotation);
        soundManager.dieSound.Play();
        Destroy(gameObject);
    }
}
