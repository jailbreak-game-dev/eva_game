using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiranhaPlant : MonoBehaviour
{
    [SerializeField] private SoundManager soundManager;
    
    [SerializeField] private GameObject star;
    [SerializeField] private int hp;

    private Animator am;
    
    private void Start()
    {
        am = GetComponent<Animator>();
    }

    private void Update()
    {
        PiranhaPlantDie();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Fireball")
        {
            hp--;
        }
    }

    private void PiranhaPlantDie()
    {
        if (hp <= 0)
        {
            StartCoroutine(DieAction());
        }
    }

    private IEnumerator DieAction()
    {
        am.SetBool("plantDie",true);

        yield return new WaitForSeconds(0.50f);
        
        Instantiate(star, transform.position, transform.rotation);
        soundManager.dieSound.Play();
        Destroy(gameObject);
    }
}
