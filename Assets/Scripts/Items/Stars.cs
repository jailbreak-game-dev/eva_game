using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EvaGame
{
    public class Stars : MonoBehaviour
    {
        
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private SoundManager soundManager;
    
    void Start()
    {

    }
    
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            soundManager.keepItemsSound.Play();
            scoreManager.score++;
            Destroy(gameObject);
        }
    }
    
    }
}