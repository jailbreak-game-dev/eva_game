using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private SoundManager soundManager;
    
    [SerializeField] private TextMeshProUGUI displayHp;
    
    [SerializeField] private int hp; 
    [SerializeField] private float speed;
    
    //position Rotation Scale
    private float x; 
    private float sx;
    private bool ks;
    
    Animator am;
    Rigidbody rb;
    
    void Start()
    {
        am = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        sx = transform.localScale.x;
    }
    
    void Update()
    {
        Move();
        PlayerDie();
        displayHp.text = $"HP: {hp}";
    }

    void Move()
    {
        x = Input.GetAxis ("Horizontal");
        am.SetFloat("speed", Abs(x));
        
        if (Input.GetButtonDown ("Jump"))
        {
            am.SetBool("jump", true);
            rb.velocity = new Vector2 (rb.velocity.x, 5f);
            soundManager.jumpSound.Play();
        }
        
        rb.velocity = new Vector2 (x * speed, rb.velocity.y);
        
        if (x > 0)
        {
            transform.localScale = new Vector3 (sx, transform.localScale.y, transform.localScale.z);
        }
        if (x < 0)
        {
            transform.localScale = new Vector3 (-sx, transform.localScale.y, transform.localScale.z);
        }
    }
    void OnCollisionEnter(Collision other)
    {
        am.SetBool("jump", false);
        soundManager.walkSound.Play();

        if (other.gameObject.tag == "Enemy")
        {
            hp --;
        }
    }
    
    float Abs(float x)
    {
        return x >= 0f ? x : -x;
    }

    private void PlayerDie()
    {
        if (hp <= 0)
        {
            soundManager.dieSound.Play();
            Destroy(gameObject);
        }
    }
    
    private void OnBecameInvisible()
    {
        Destroy (gameObject);
    }
}
