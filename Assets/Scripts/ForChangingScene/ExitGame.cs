using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGame : MonoBehaviour
{
    public void DoGameExit()
    {
        Application.Quit();
        Debug.Log("Player has quitted the game");
    }
}
