using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
//using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class PlayAgain_Retry : MonoBehaviour
{
    [SerializeField]
    private GameObject myCanvas;

    private int normalTime = 1;
    private int freezeTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        Button button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(PlayAgainOrTryAgain);
    }

    void PlayAgainOrTryAgain()
    {
        Time.timeScale = normalTime;
        myCanvas.SetActive(false);
        //Scene thisActiveScene = SceneManager.GetActiveScene();
        //SceneManager.LoadScene(thisActiveScene.name);
        Application.LoadLevel(Application.loadedLevel);
    }
}
