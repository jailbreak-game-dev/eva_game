using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerFire : MonoBehaviour
{
    [SerializeField] private SoundManager soundManager;
    
    [SerializeField] private GameObject fire;

    [SerializeField] private Transform fireSpawner;
    
    
    private float fireRate;
    private float nextFire;
    
    void Update()
    {
        FireButton();
    }

    private void FireButton()
    {
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(fire, fireSpawner.position, fireSpawner.rotation);
            soundManager.fireSound.Play();
        }
    }
    
    private void OnBecameInvisible()
    {
        Destroy (gameObject);
    }
}
