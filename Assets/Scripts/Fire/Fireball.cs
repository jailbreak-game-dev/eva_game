using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.PlayerConnection;

public class Fireball : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Transform player;

    private Rigidbody rb;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        if (player.transform.localScale.x > 0)
        {
            rb.AddForce(speed,0,0);
        }
        if (player.transform.localScale.x < 0)
        {
            rb.AddForce(-speed,0,0);
        }
    }
    
    private void OnBecameInvisible()
    {
        Destroy (gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy (gameObject);
    }
}
